use std::{fs, path::Path};
use image::io::Reader as ImageReader;
use clap::{Parser};
use image::ImageFormat;

#[derive(Parser)]
/// converts a webp <SOURCE> to a jpeg optionally deleting the source file
struct Cli {
    #[clap(short, long)]
    verbose: bool,

    #[clap(long)]
    delete: bool,

    /// file to convert
    source: String,
}

fn main() {
    let cli = Cli::parse();

    let source = Path::new(&cli.source).canonicalize().unwrap();

    if !source.is_file() {
        eprintln!("please pass a webp file to convert");
        return;
    }

    let mut target = source.clone();
    target.set_extension("jpg");

    let mut suffix = 1;
    let original_filestem = match source.file_stem() {
        None => "image",
        Some(oss) => match oss.to_str() {
            None => "image",
            Some(s) => s,
        },
    };

    while target.exists() {
        let lets_try_this = format!("{}-{}.jpg", original_filestem, suffix);
        target.set_file_name(lets_try_this);
        suffix +=1;
    }

    if target.exists() {
        eprintln!("target already exists, bailing");
        return;
    }

    let img = ImageReader::open(source.clone()).expect("could not load source file").decode().expect("could not decode source image");
    img.save_with_format(target, ImageFormat::Jpeg).expect("could not encode target image");

    if cli.delete {
        fs::remove_file(&source).expect("could not remove source");
    }
}
